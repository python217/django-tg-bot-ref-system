from django.contrib import admin
from .forms import ProfileForm
from .models import Profile

@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ('id', 'referral_id', 'user_id', 'user_name')
    form = ProfileForm