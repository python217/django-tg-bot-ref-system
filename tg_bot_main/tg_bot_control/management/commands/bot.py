from django.core.management.base import BaseCommand
from django.conf import settings
from telegram import Bot
from telegram.utils.request import Request
from telegram.ext import Updater
from telegram.ext import MessageHandler
from telegram.ext import Filters
from telegram.ext import CallbackContext
from telegram import Update


from tg_bot_control.models import Profile


def log_errors(f):
    
    def inner(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as e:
            error_message = f'Error: {e}'
            print(error_message)
            raise e
    return inner


@log_errors
def do_echo(update: Update, context: CallbackContext):
    chat_id = update.message.chat_id
    text = update.message.text
    
    if text.split()[0] == '/start':
        try:
            get_message_id = text.split()[1]
            one_entry = Profile.objects.filter(
                referral_id=get_message_id
                ).order_by('user_name').get()
            one_entry_name = f"{one_entry}".split()[1]
            one_entry_id = f"{one_entry}".split()[0]
            
            answer = f"Hello {one_entry_name}. Your ID - {one_entry_id}"
        except Exception as e:
            answer = f"ERROR. Contact with developers. {e}"
    else:
        answer = "This bot only for ref users. Try again"
    
    reply_text = "Chat ID = {}\n\n{}".format(chat_id, answer)
    update.message.reply_text(
        text=reply_text,
    )


class Command(BaseCommand):
    help = 'Teg bot'
    
    def handle(self, *args, **options):
        request = Request(
            connect_timeout=0.5,
            read_timeout=1.0,
        )
        
        bot = Bot (
            request=request,
            token=settings.TOKEN,
        )
        print(bot.get_me())
        
        updater = Updater(
            bot=bot,
            use_context=True
        )
        
        
        
        message_handler = MessageHandler(Filters.text, do_echo)
        updater.dispatcher.add_handler(message_handler)
        
        updater.start_polling()
        updater.idle()