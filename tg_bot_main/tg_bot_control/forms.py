from django import forms
from django.forms import widgets
from .models import Profile

class ProfileForm(forms.ModelForm):
    
    class Meta:
        model = Profile
        fields = (
            'referral_id',
            'user_id',
            'user_name',
        )
        widgets = {
            'user_name': forms.TextInput,
        }