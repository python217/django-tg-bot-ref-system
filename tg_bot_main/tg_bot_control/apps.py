from django.apps import AppConfig


class TgBotControlConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tg_bot_control'
