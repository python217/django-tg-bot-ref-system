import profile
from django.db import models

class Profile(models.Model):
    referral_id = models.PositiveBigIntegerField(verbose_name='Ref user ID')
    user_id = models.PositiveIntegerField(verbose_name='User ID', unique=True)
    user_name = models.TextField(verbose_name='User name')
    
    def __str__(self):
        return f'{self.user_id} {self.user_name}'
    
    class Meta:
        verbose_name = 'Profile'
        
    
